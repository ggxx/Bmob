package com.bmobxiaowa.api;

import han.k.common.KLoger;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.bmobxiaowa.api.GetData.OnHttpDateStringListener;
import com.bmobxiaowa.entity.Gui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * 
 * Created by hanWG on 2015-11-13 下午4:19:26 $
 * 
 * @Description 只返回Http源码
 *              <p/>
 */
public class GetHttp {
	HttpResponse response;
	HttpClient client = new DefaultHttpClient();

	/**
	 * 开启线程执行请求得到网页字符串
	 */
	public void getHttpHtml(final String url) {
		KLoger.d("==加载下一页==", url);
		new Thread() {
			@Override
			public void run() {
				super.run();
				HttpGet request = new HttpGet(url.trim());
				try {
					response = client.execute(request);
					// 请求成功
					if (response.getStatusLine().getStatusCode() == 200) {
						byte[] bytearray = EntityUtils.toByteArray(response
								.getEntity());
						String html = new String(bytearray, "GBK");
						Bundle bundle = new Bundle();
						bundle.putString("html", html);
						Message msg = handler.obtainMessage();
						msg.setData(bundle);
						handler.sendMessage(msg);
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();

	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			String html = msg.getData().getString("html");
			//返回htnl代码
			onhttphtmlstringlistener.onHttpDate(html);
		}

	};
	public void setOnHttpHtmlStringListener(
			OnHttpHtmlStringListener onhttphtmlstringlistener) {
		this.onhttphtmlstringlistener = onhttphtmlstringlistener;

	}
	public OnHttpHtmlStringListener onhttphtmlstringlistener;
	public interface OnHttpHtmlStringListener {
		void onHttpDate(String string);
	}
}
