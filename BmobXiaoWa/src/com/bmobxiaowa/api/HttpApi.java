package com.bmobxiaowa.api;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.bmobxiaowa.entity.Gui;

public class HttpApi {

	public static ArrayList<Gui> getGuiList(String html) {
		ArrayList<Gui> list = new ArrayList<Gui>();
		Document doc = Jsoup.parse(html);
		Element bodys = doc.body();
		Elements content = bodys.getElementsByClass("main").get(0)
				.getElementsByClass("left").get(0).getElementsByClass("list")
				.get(0).getElementsByTag("ul");
		// 遍历简介
		for (Element contents : content) {
			Elements title = contents.getElementsByTag("li");
			for (Element titles : title) {
				Gui gui = new Gui();
				// 获取到标题
				String gtitle = titles.getElementsByTag("h1").get(0)
						.getElementsByTag("a").text();
				String context = titles.getElementsByTag("p").get(0).text();
				String urls = titles.getElementsByTag("h1").get(0)
						.getElementsByTag("a").get(0).attr("href");

				gui.setTxtUrl(urls);
				gui.setGtitle(gtitle);
				gui.setGcontext(context);
				list.add(gui);
			}

		}
		return list;
	}
}
