package com.bmobxiaowa.api;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;

import com.bmobxiaowa.entity.Gui;

/**
 * 获取鬼大爷 代码
 */
public class HttpGetGui {
	HttpResponse response;
	HttpClient client = new DefaultHttpClient();
	private ArrayList<Gui> list;

	/*
	 * 获取具体某一页内容
	 */
	public Gui getHtml(String url,String deletes) {
		Gui gui = new Gui();
		// 页数
		String pages = "0";
		HttpGet request = new HttpGet(url);
		try {
			response = client.execute(request);
			// 请求成功
			if (response.getStatusLine().getStatusCode() == 200) {
				byte[] bytearray = EntityUtils
						.toByteArray(response.getEntity());
				String html = new String(bytearray, "GBK");
				Document doc = Jsoup.parse(html);
				Element bodys = doc.body();
				Elements content = bodys.getElementsByClass("main").get(0)
						.getElementsByClass("left");
				
				//得到文本数据
				 Element text = content.get(0).getElementsByClass("content").get(0).getElementsByClass("text").get(0);
				 text.select("div.neirongs").remove();
				 text.select("div."+deletes).remove();
				System.out.println(text.text());
				 Elements page = content.get(0).getElementsByClass("content").get(0).getElementsByClass("page");
				if ("".equals(page.text())) {
					pages ="0";
				}else{
					//得到总页数
					pages = page.get(0).getElementsByTag("a").get(1).text();
				//	System.out.println(p.toString());
				}
				gui.setTxtPage(pages);
				gui.setTxt(text.text());

				return gui;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		return null;
	}

	/**
	 * 获取主页列表数据
	 */
	public ArrayList<Gui> getMainList(String url) {
		list = new ArrayList<Gui>();
		HttpGet request = new HttpGet(url.trim());
		try {
			response = client.execute(request);
			// 请求成功
			if (response.getStatusLine().getStatusCode() == 200) {
				byte[] bytearray = EntityUtils
						.toByteArray(response.getEntity());
				String html = new String(bytearray, "GBK");
				Document doc = Jsoup.parse(html);
				Element bodys = doc.body();
				Elements content = bodys.getElementsByClass("main").get(0)
						.getElementsByClass("left").get(0)
						.getElementsByClass("list").get(0)
						.getElementsByTag("ul");
				// 遍历简介
				for (Element contents : content) {
					Elements title = contents.getElementsByTag("li");
					for (Element titles : title) {
						Gui gui = new Gui();
						// 获取到标题
						String gtitle = titles.getElementsByTag("h1").get(0)
								.getElementsByTag("a").text();
						String context = titles.getElementsByTag("p").get(0)
								.text();
						String urls = titles.getElementsByTag("h1").get(0)
								.getElementsByTag("a").get(0).attr("href");
						
						
						gui.setTxtUrl(urls);
						gui.setGtitle(gtitle);
						gui.setGcontext(context);
						list.add(gui);
					}

				}
				return list;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void main(String[] args) {

		new HttpGetGui().getMainList("http://www.guidaye.com/dp/index_2.html");
	}

}
