package com.bmobxiaowa.api;

/**
 * Created by hanWG on 2015-11-11 下午5:40:46 $
 * 
 * @Description EventBus 标识
 *              <p/>
 */
public class EventBusTag {
	public static final String ASYNC_TAG_GUI = "async_guidayeneir";
	public static final String FILEMANAGEFRAGMENT = "filemanagefragment";// 文件fragment
	public static final String DATATBFRAGMENT = "DataTbFragment";// 数据同步操作
	public static final String DATAFRAGMENT = "DataFragment";// 数据批量操作

}
