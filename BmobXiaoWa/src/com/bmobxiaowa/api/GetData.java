package com.bmobxiaowa.api;

import java.io.Serializable;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;

import com.bmobxiaowa.entity.Gui;

/**
 * Created by hanWG on 2015-11-11 下午4:03:09 $
 * 
 * @Description 获取数据类封装
 *              <p/>
 */
public class GetData {
	private HttpGetGui gui;
	// 获得集合数据
	public GetData getlist(final String url) {
		gui = new HttpGetGui();
		new Thread() {
			public void run() {
				ArrayList<Gui>	list = gui.getMainList(url);
				Message msg = hander.obtainMessage();
				Bundle bundle = new Bundle();
				bundle.putSerializable("list", (Serializable)list);
				msg.setData(bundle);
				hander.sendMessage(msg);
			};
		}.start();
		return this;
	}

	// 获得字符串
	public GetData getHtml(final String url,final String deletes) {
		gui = new HttpGetGui();
		new Thread() {
			public void run() {
				Gui guidayes = gui.getHtml(url,deletes);
				Bundle bundle = new Bundle();
				bundle.putSerializable("gui", guidayes);
				Message msg = hander.obtainMessage();
				msg.setData(bundle);
				hander.sendMessage(msg);
			};
		}.start();
		return this;
	}

	Handler hander = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (onHttpDateListener != null) {
				ArrayList<Gui>  lists = (ArrayList<Gui>) msg.getData().getSerializable("list");
				onHttpDateListener.onHttpDate(lists);
			}
			if (OnHttpDateStringListener != null) {
				Gui  guidayes =  (Gui) msg.getData().getSerializable("gui");
				OnHttpDateStringListener.onHttpDate(guidayes);
			}

		};
	};

	public void setOnHttpDateListener(OnHttpDateListener onHttpDateListener) {
		this.onHttpDateListener = onHttpDateListener;
	}

	public void setOnHttpDateStringListener(
			OnHttpDateStringListener OnHttpDateStringListener) {
		this.OnHttpDateStringListener = OnHttpDateStringListener;
	}

	public OnHttpDateListener onHttpDateListener;
	public OnHttpDateStringListener OnHttpDateStringListener;

	public interface OnHttpDateListener {
		void onHttpDate(ArrayList<Gui> list);
	}

	public interface OnHttpDateStringListener {
		void onHttpDate(Gui gui);
	}

}
