package com.bmobxiaowa.util;

import han.k.util.KPopView;
import han.k.util.KpaiZ;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.fragment.FileManageFragment;
import com.bmobxiaowa.fragment.ImageFragment;

/**
 * Created by hanWG on 2015-11-23 下午3:52:08 $
 * 
 * @Description <p/>
 */
public class DialogImage {
	public static int REQUEST_CODE_PHOTOGRAPH = 11;
	public static int REQUEST_CODE_LOCPIC = 12;
	public static int REQUEST_CODE_HISIPIN = 13;
	Dialog d;

	public Dialog showDialog(final FileManageFragment fragment,
			final Activity aty, final KpaiZ kpz) {
		View view = LayoutInflater.from(aty).inflate(R.layout.d_view, null);
		/* 选择照片 */
		view.findViewById(R.id.xuanze).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {

						Intent intent = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						fragment.startActivityForResult(intent,
								REQUEST_CODE_LOCPIC);
						d.dismiss();
					}
				});
		/* 选择其他文件 */
		view.findViewById(R.id.xuanzesp).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent i = new Intent(Intent.ACTION_GET_CONTENT);
						i.setType("file/*");
						fragment.startActivityForResult(i, REQUEST_CODE_HISIPIN);
						d.dismiss();
					}
				});
		/* 拍照 */
		view.findViewById(R.id.paizhao).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intents = kpz.pai();
						fragment.startActivityForResult(intents,
								REQUEST_CODE_PHOTOGRAPH);
						d.dismiss();
					}
				});
		return d = new KPopView().getDialogView(aty, view);
	}

}
