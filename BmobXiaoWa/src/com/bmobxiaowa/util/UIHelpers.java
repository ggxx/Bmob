package com.bmobxiaowa.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bmobxiaowa.activity.ContextActivity;
import com.bmobxiaowa.activity.LoginActivity;
import com.bmobxiaowa.activity.MainActivity;
import com.bmobxiaowa.activity.RegActivity;
import com.bmobxiaowa.activity.file.FileChooserActivity;
import com.bmobxiaowa.entity.Gui;
import com.bmobxiaowa.fragment.DataFragment;
import com.bmobxiaowa.fragment.FileManageFragment;
import com.bmobxiaowa.fragment.GuiFragment;
import com.bmobxiaowa.fragment.ImageFragment;
import com.bmobxiaowa.fragment.MainFragment;
import com.bmobxiaowa.fragment.MyDataFragment;
import com.bmobxiaowa.fragment.DataDanGeFragment;

/**
 * 
 * Created by hanWG on 2015-11-10 下午5:18:28 $
 * 
 * @Description 界面辅助类
 *              <p/>
 *              跳转方法，依赖倒置
 */
public class UIHelpers {
	/**
	 * 显示登录界面
	 * 
	 * @param context
	 */
	public static void showLoginActivity(Context context) {
		Intent intent = new Intent(context, LoginActivity.class);
		context.startActivity(intent);
	}

	/**
	 * 进入选择文件的界面
	 * 
	 * @param context
	 */
	public static void showFileChooserActivity(Context context) {
		Intent intent = new Intent(context, FileChooserActivity.class);
		context.startActivity(intent);
	}

	/**
	 * 显示注册界面
	 * 
	 * @param context
	 */
	public static void showRegActivity(Context context) {
		Intent intent = new Intent(context, RegActivity.class);
		context.startActivity(intent);
	}

	/**
	 * 显示主界面
	 * 
	 * @param context
	 */
	public static void showMainActivity(Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		context.startActivity(intent);
	}

	/**
	 * 显示内容主界面
	 * 
	 * @param context
	 */
	public static void showContexActivity(Context context, Gui gui) {
		Intent intent = new Intent(context, ContextActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("user", gui);
		intent.putExtras(bundle);
		context.startActivity(intent);
	}

	/**
	 * 获取mainfragment
	 * 
	 * @return
	 */
	public static MainFragment getMainFragment() {
		MainFragment fragment = MainFragment.newInstance();
		return fragment;
	}

	/**
	 * 获取文件管理
	 * 
	 * @return
	 */
	public static FileManageFragment getFileManageFragment() {
		FileManageFragment fragment = FileManageFragment.newInstance();
		return fragment;
	}

	/**
	 * 获取Gui
	 * 
	 * @return
	 */
	public static GuiFragment getGuiFragment() {
		// GuiFragment fragment = GuiFragment.newInstance();
		GuiFragment fragment = GuiFragment.newInstance();
		return fragment;
	}

	/**
	 * 获取Image
	 * 
	 * @return
	 */
	public static ImageFragment getImageFragment() {
		ImageFragment fragment = ImageFragment.newInstance();
		return fragment;
	}

	/**
	 * 获取 二级列表
	 * 
	 * @return
	 */
	public static DataDanGeFragment getDataTBFragment() {
		DataDanGeFragment fragment = DataDanGeFragment.newInstance();
		return fragment;
	}
	/**
	 * 数据操作
	 * 
	 * @return
	 */
	public static DataFragment getDataFragment() {
		DataFragment fragment = DataFragment.newInstance();
		return fragment;
	}
	/**
	 * 我的数据展示
	 * 
	 * @return
	 */
	public static MyDataFragment getMyDataFragment() {
		MyDataFragment fragment = MyDataFragment.newInstance();
		return fragment;
	}

}
