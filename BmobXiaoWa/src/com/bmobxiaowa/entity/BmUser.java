package com.bmobxiaowa.entity;

import cn.bmob.v3.BmobUser;

public class BmUser extends BmobUser {
	private static final long serialVersionUID = -4830698523597287195L;
	private String info;
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}

}
