package com.bmobxiaowa.entity;

import java.io.Serializable;

import cn.bmob.v3.BmobObject;

/**
 * Created by hanWG on 2015-11-11 上午11:42:59 $
 * 
 * @Description 鬼大爷实体类
 *              <p/>
 */
public class Gui extends BmobObject implements Serializable{
	private static final long serialVersionUID = 2987664438095045730L;
	private String txt;// 内容
	private String txtPage;// 内容页数
	private String gtitle;// 标题
	private String gcontext;// 简介
	private String txtUrl;// 具体内容Url

	/**
	 * @return txtPage
	 */
	public String getTxtPage() {
		return txtPage;
	}

	/**
	 * @param txtPage 要设置的 txtPage
	 */
	public void setTxtPage(String txtPage) {
		this.txtPage = txtPage;
	}

	/**
	 * @return txtUrl
	 */
	public String getTxtUrl() {
		return txtUrl;
	}

	/**
	 * @param txtUrl
	 *            要设置的 txtUrl
	 */
	public void setTxtUrl(String txtUrl) {
		this.txtUrl = txtUrl;
	}

	/**
	 * @return txt
	 */
	public String getTxt() {
		return txt;
	}

	/**
	 * @param txt
	 *            要设置的 txt
	 */
	public void setTxt(String txt) {
		this.txt = txt;
	}

	public String getGtitle() {
		return gtitle;
	}

	public void setGtitle(String gtitle) {
		this.gtitle = gtitle;
	}

	public String getGcontext() {
		return gcontext;
	}

	public void setGcontext(String gcontext) {
		this.gcontext = gcontext;
	}


}
