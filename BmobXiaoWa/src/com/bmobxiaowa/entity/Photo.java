package com.bmobxiaowa.entity;

import java.io.Serializable;

import cn.bmob.v3.BmobObject;

/**
 * Created by hanWG on 2015-11-11 上午11:42:59 $
 * 
 * @Description 图片实体类
 *              <p/>
 */
public class Photo extends BmobObject implements Serializable {
	private static final long serialVersionUID = -8131099244357584132L;
	private String photoUrl;// 图片地址
	private String photoSort;// 所属分类
	private String photoName;// 图片名称

	/**
	 * @return photoUrl
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}

	/**
	 * @param photoUrl
	 *            要设置的 photoUrl
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	/**
	 * @return photoSort
	 */
	public String getPhotoSort() {
		return photoSort;
	}

	/**
	 * @param photoSort
	 *            要设置的 photoSort
	 */
	public void setPhotoSort(String photoSort) {
		this.photoSort = photoSort;
	}

	/**
	 * @return photoName
	 */
	public String getPhotoName() {
		return photoName;
	}

	/**
	 * @param photoName
	 *            要设置的 photoName
	 */
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

}
