package com.bmobxiaowa;

import java.io.File;

import android.util.DisplayMetrics;
import cn.bmob.v3.Bmob;

import com.bmobxiaowa.api.BmobApi;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import han.k.base.BaseApplication;

public class BmoboApplication extends BaseApplication {
	@Override
	public void onCreate() {
		super.onCreate();
		// 注册bmob
		Bmob.initialize(this, BmobApi.APP_ID);
		File cacheDir = StorageUtils.getOwnCacheDirectory(
				getApplicationContext(), "imageloader/Cache");
		// 初始化Android-Universal-Image-Loader图片加载框架
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.diskCache(new UnlimitedDiscCache(cacheDir))
				// 自定义缓存路径
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				// 将保存的时候的URI名称用MD5 加密
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		ImageLoader.getInstance().init(config);
	}

	private static DisplayMetrics dm = new DisplayMetrics();

	public static DisplayMetrics getDisplayMetrics() {
		return dm;
	}
}
