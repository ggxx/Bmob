package com.bmobxiaowa.activity;

import han.k.common.KAtyManager;
import han.k.view.K_TitleBar;
import han.k.widget.menu.ResideMenu;
import han.k.widget.menu.ResideMenuItem;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.base.BomBaseActivity;
import com.bmobxiaowa.service.MyService;
import com.bmobxiaowa.util.UIHelpers;
import com.flyco.dialog.listener.OnBtnLeftClickL;
import com.flyco.dialog.listener.OnBtnRightClickL;
import com.flyco.dialog.widget.NormalDialog;

/**
 * Created by hanWG on 2015-11-10 下午5:26:45 $
 * 
 * @Description <p/>
 */
public class MainActivity extends BomBaseActivity implements OnClickListener {
    private ResideMenu resideMenu;// 左侧菜单
    private ResideMenuItem menuItem;
    private ResideMenuItem menuItem1;
    private ResideMenuItem mI2;
    private ResideMenuItem mI3;
    private ResideMenuItem mI4;
    private ResideMenuItem mI5;
    private ResideMenuItem mI6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeFragment(R.id.main_content, UIHelpers.getMainFragment());
        initTitleBar();
        setUpMenu();
        Intent intent = new Intent();
        intent.setClass(this, MyService.class);
        startService(intent);
    }

    /**
	 * 
	 */
    private void initTitleBar() {
        K_TitleBar titleBar = (K_TitleBar) findViewById(R.id.title_bar);
        titleBar.setImmersive(false);
        titleBar.setLeftImageResource(R.drawable.back_green);
        titleBar.setBackgroundColor(Color.parseColor("#619158"));
        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        titleBar.setTitle("Bmob数据管理");
        titleBar.setTitleColor(Color.WHITE);
        titleBar.setDividerColor(Color.GRAY);
    }

    // 设置左侧菜单
    private void setUpMenu() {
        resideMenu = new ResideMenu(MainActivity.this);
        resideMenu.setUse3D(false);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setScaleValue(0.55f);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);// 设置关闭左滑菜单
        menuItem = new ResideMenuItem(this, R.drawable.ic_launcher, "简介");
        menuItem1 = new ResideMenuItem(this, R.drawable.ic_launcher, "上传测试>");
        mI2 = new ResideMenuItem(this, R.drawable.ic_launcher, "文件管理>");
        mI3 = new ResideMenuItem(this, R.drawable.ic_launcher, "数据单个操作>");
        mI4 = new ResideMenuItem(this, R.drawable.ic_launcher, "消息推送>");
        mI5 = new ResideMenuItem(this, R.drawable.ic_launcher, "数据批量操作>");
        mI6 = new ResideMenuItem(this, R.drawable.ic_launcher, "图片数据展示>");
        resideMenu.addMenuItem(menuItem, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(menuItem1, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(mI2, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(mI3, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(mI4, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(mI5, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(mI6, ResideMenu.DIRECTION_LEFT);
        menuItem.setOnClickListener(this);
        menuItem1.setOnClickListener(this);
        mI2.setOnClickListener(this);
        mI3.setOnClickListener(this);
        mI5.setOnClickListener(this);
        mI6.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        resideMenu.closeMenu();
        if (v == mI2) {
            // rchangeFragment(R.id.main_content, UIHelpers.getImageFragment());
            rchangeFragment(R.id.main_content, UIHelpers.getFileManageFragment());
        } else if (v == menuItem) {
            rchangeFragment(R.id.main_content, UIHelpers.getMainFragment());
        } else if (v == menuItem1) {
            rchangeFragment(R.id.main_content, UIHelpers.getGuiFragment());
        } else if (v == mI3) {
            rchangeFragment(R.id.main_content, UIHelpers.getDataTBFragment());
        } else if (v == mI5) {
            rchangeFragment(R.id.main_content, UIHelpers.getDataFragment());
        } else if (v == mI6) {
            // 数据展示操作
            rchangeFragment(R.id.main_content, UIHelpers.getMyDataFragment());
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dialog();
        }
        return false;
    }

    private void dialog() {

        final NormalDialog dialog = new NormalDialog(this);
        dialog.isTitleShow(false)
                        .bgColor(Color.parseColor("#383838")).cornerRadius(5).content("是否确定退出程序?")
                        .contentGravity(Gravity.CENTER)
                        .contentTextColor(Color.parseColor("#ffffff"))
                        .dividerColor(Color.parseColor("#222222")).btnTextSize(15.5f, 15.5f)
                        .btnTextColor(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"))//
                        .btnColorPress(Color.parseColor("#2B2B2B"))//
                        .widthScale(0.85f).show();
        dialog.setOnBtnLeftClickL(new OnBtnLeftClickL() {
            @Override
            public void onBtnLeftClick() {
                dialog.dismiss();
                KAtyManager.create().finishAllActivity();
            }
        });
        dialog.setOnBtnRightClickL(new OnBtnRightClickL() {
            @Override
            public void onBtnRightClick() {
                dialog.dismiss();
            }
        });
    }
}
