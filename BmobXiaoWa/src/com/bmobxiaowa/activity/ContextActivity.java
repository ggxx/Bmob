package com.bmobxiaowa.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.api.AppUrl;
import com.bmobxiaowa.api.GetData;
import com.bmobxiaowa.api.GetData.OnHttpDateStringListener;
import com.bmobxiaowa.base.BomBaseActivity;
import com.bmobxiaowa.entity.Gui;

/**
 * Created by hanWG on 2015-11-11 下午5:02:15 $
 * 
 * @Description 文件主界面
 *              <p/>
 */
public class ContextActivity extends BomBaseActivity implements OnClickListener {
	private GetData getd;
	private TextView tv;
	private TextView title;
	private Gui gui;
	private String delets;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_context);
		title = (TextView) findViewById(R.id.title);
		findViewById(R.id.submit).setOnClickListener(this);
		tv = (TextView) findViewById(R.id.tv);
		getd = new GetData();
		Intent intent = this.getIntent();
		gui = (Gui) intent.getSerializableExtra("user");
		delets = gui.getTxtUrl().split("\\.")[0].split("/")[2]; // 删除数据
		dialog_tv.setText("正在获取数据");
		dialog.show();
		// 获取数据
		getd.getHtml(AppUrl.GUIDAYEuRL + gui.getTxtUrl(), delets)
				.setOnHttpDateStringListener(new OnHttpDateStringListener() {
					private StringBuffer sbf;
					@Override
					public void onHttpDate(Gui guis) {
						dialog.dismiss();
						// 读取下一页
						if (!"0".equals(guis.getTxtPage())) {
							sbf = new StringBuffer();
							sbf.append(guis.getTxt());
							String as = gui.getTxtUrl().split("\\.")[0];
							getd = new GetData();
							getd.getHtml(AppUrl.GUIDAYEuRL + as + "_2.html",
									delets).setOnHttpDateStringListener(
									new OnHttpDateStringListener() {
										@Override
										public void onHttpDate(Gui guis_) {
											sbf.append(guis_.getTxt());
											title.setText(gui.getGtitle());
											tv.setText(sbf.toString());
										}
									});
						} else {// 显示
							title.setText(gui.getGtitle());
							tv.setText(guis.getTxt());
						}
					}
				});
	}
	@Override
	public void onClick(View v) {
		// 保存之前先查询
		switch (v.getId()) {
		case R.id.submit:
			selectGui();

			break;

		default:
			break;
		}
	}

	private void selectGui() {
		// 查找Person表里面id为6b6c11c537的数据
		BmobQuery<Gui> bmobQuery = new BmobQuery<Gui>();
		bmobQuery.addWhereEqualTo("txtUrl", gui.getTxtUrl());
		bmobQuery.findObjects(this, new FindListener<Gui>() {
			@Override
			public void onSuccess(List<Gui> object) {
				if (object.size() > 0) {
					toast_("数据已经保存!");
				} else {
					dialog_tv.setText("正在保存数据");
					dialog.show();
					gui.setTxt(tv.getText().toString());
					gui.save(ContextActivity.this, new SaveListener() {
						@Override
						public void onSuccess() {
							toast_("保存成功");
							dialog.dismiss();
						}

						@Override
						public void onFailure(int arg0, String arg1) {
							toast_("保存失败" + arg1);
							dialog.dismiss();
						}
					});
				}
			}

			@Override
			public void onError(int code, String msg) {
				toast_("--->>>失败");
			}
		});
	}

}
