package com.bmobxiaowa.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import cn.bmob.v3.listener.SaveListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.base.BomBaseActivity;
import com.bmobxiaowa.entity.BmUser;

/**
 * Created by hanWG on 2015-11-10 下午4:58:20 $
 * 
 * @Description <p/>
 */
public class RegActivity extends BomBaseActivity  implements OnClickListener{
	private EditText et_name;
	private EditText et_password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg);
		et_name = (EditText) this.findViewById(R.id.et_name);
		et_password = (EditText) this.findViewById(R.id.et_password);
		Button login = (Button) findViewById(R.id.login);
		login.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login:
			BmUser user = new BmUser();
			user.setUsername(et_name.getText().toString().trim());
			user.setPassword(et_password.getText().toString().trim());
			user.setInfo("a");
			user.signUp(this, new SaveListener() {
				@Override
				public void onSuccess() {
					toast_("注册成功");
				}

				@Override
				public void onFailure(int arg0, String arg1) {
					toast_("注册失败" + arg1);
				}
			});
			break;
		default:
			break;
		}

	}

}
