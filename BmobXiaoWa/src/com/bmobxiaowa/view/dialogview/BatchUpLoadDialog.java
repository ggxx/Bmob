package com.bmobxiaowa.view.dialogview;

import han.k.common.KLoger;
import han.k.common.KNoDoubleClickListener;
import han.k.common.KToast;

import java.util.ArrayList;
import java.util.List;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.bmob.v3.datatype.BmobFile;

import com.bmob.BmobProFile;
import com.bmob.btp.callback.UploadBatchListener;
import com.bmobxiaowa.R;
import com.bmobxiaowa.activity.MainActivity;
import com.bmobxiaowa.db.DaoService;
import com.bmobxiaowa.entity.Photo;
import com.bmobxiaowa.fragment.photo.PhotoInfo;
import com.bmobxiaowa.util.FileUtul;
import com.bmobxiaowa.util.UIHelpers;
import com.flyco.dialog.widget.base.BottomBaseDialog;

/**
 * Created by hanWG on 2015-11-27 下午2:46:29 $
 * 
 * @Description 批量上传
 *              <p/>
 */
public class BatchUpLoadDialog extends BottomBaseDialog {

	private TextView data_tv;
	private ListView listviewid;
	private ArrayList<String> photodata;
	private ArrayAdapter<String> myArrayAdapter;
	private MainActivity aty;
	private boolean flag = true;
	private DaoService daoservcie;

	/**
	 * @param context
	 */
	public BatchUpLoadDialog(Context context) {
		super(context);
	}

	public BatchUpLoadDialog(Context context, View animateView) {
		super(context, animateView);
		aty = (MainActivity) context;
	}

	@Override
	public View onCreateView() {
		// 注册EventBus
		EventBus.getDefault().register(this);
		widthScale(1);
		heightScale(0.55f);
		View contextView = View.inflate(context, R.layout.view_batchupload,
				null);

		contextView.findViewById(R.id.button1).setOnClickListener(
				new KNoDoubleClickListener() {
					@Override
					public void onNoDoubleClick(View v) {
						// 弹出选择图片的activity
						UIHelpers.showFileChooserActivity(getContext());
					}
				});

		daoservcie = new DaoService(aty);
		contextView.findViewById(R.id.button2).setOnClickListener(
				new KNoDoubleClickListener() {
					@Override
					public void onNoDoubleClick(View v) {
						if (photodata != null) {
							if (flag) {
								aty.dialog.show();
							}
							aty.dialog_tv.setText("正在上传");
							final int size = photodata.size();
							String[] files = (String[]) photodata
									.toArray(new String[size]);
							for (int i = 0; i < files.length; i++) {
								String string = files[i];
								KLoger.d("==上传==", "上传--" + string);

							}
							BmobProFile.getInstance(context).uploadBatch(files,
									new UploadBatchListener() {

										@Override
										public void onSuccess(boolean isFinish,
												String[] fileNames,
												String[] urls, BmobFile[] files) {
											// isFinish ：批量上传是否完成
											// fileNames：文件名数组
											// urls : url：文件地址数组
											// files :
											// BmobFile文件数组，`V3.4.1版本`开始提供，用于兼容新旧文件服务。
											if (isFinish) {
												KLoger.d("==shuzu==", ""
														+ fileNames.length);
												KToast.showToast(aty, "上传成功！",
														Toast.LENGTH_SHORT);
												aty.dialog.dismiss();
												for (int i = 0; i < files.length; i++) {
													BmobFile bmobFile = files[i];
													String Imageurl = bmobFile
															.getFileUrl(aty);
													KLoger.d("==urls==",
															Imageurl);
													// 把上传的文件名存入到文件
													Photo photo=new Photo();
													photo.setPhotoUrl(Imageurl);
													photo.setPhotoName(bmobFile.getFilename());
													photo.setPhotoSort(FileUtul.getFileSuff(Imageurl));
													if (photo!=null) {
														daoservcie.savePhoto(photo);
													}
												}
											}
										}

										@Override
										public void onProgress(int curIndex,
												int curPercent, int total,
												int totalPercent) {
											// curIndex :表示当前第几个文件正在上传
											// curPercent :表示当前上传文件的进度值（百分比）
											// total :表示总的上传文件数
											// totalPercent:表示总的上传进度（百分比）

											data_tv.setText("选择了"
													+ photodata.size() + "张图片,"
													+ "正在上传:" + curIndex + "/"
													+ total);
										}

										@Override
										public void onError(int statuscode,
												String errormsg) {
											KToast.showToast(aty, "上传失败！",
													Toast.LENGTH_SHORT);
											aty.dialog.dismiss();
										}
									});
							
						}
					}
				});
		data_tv = (TextView) contextView.findViewById(R.id.data_tv);
		listviewid = (ListView) contextView.findViewById(R.id.listviewid);
		return contextView;
	}

	@Subscriber(tag = "selectPhotoData")
	public void selectPhotoDatas(List<PhotoInfo> selectPhotoData) {
		photodata = new ArrayList<String>();
		for (int i = 0; i < selectPhotoData.size(); i++) {
			// 判断是否是选中的
			if (selectPhotoData.get(i).isChoose()) {
				photodata.add(selectPhotoData.get(i).getPath_file());
			}
		}
		myArrayAdapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_list_item_1, photodata);
		listviewid.setAdapter(myArrayAdapter);
		data_tv.setText("选择了" + photodata.size() + "张图片");
	}

	@Override
	public boolean setUiBeforShow() {
		return false;
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
		if (daoservcie!=null) {
			daoservcie.close();
		}
	}
}