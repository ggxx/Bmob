package com.bmobxiaowa.view.dialogview;

import han.k.common.KGlide;
import han.k.common.KLoger;
import han.k.common.KNoDoubleClickListener;
import han.k.common.KToast;
import han.k.util.KpaiZ;
import han.k.view.K_LoadingCircleView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.bmob.v3.datatype.BmobFile;

import com.bmob.BTPFileResponse;
import com.bmob.BmobProFile;
import com.bmob.btp.callback.UploadListener;
import com.bmobxiaowa.R;
import com.bmobxiaowa.activity.MainActivity;
import com.bmobxiaowa.db.DaoService;
import com.bmobxiaowa.entity.Photo;
import com.bmobxiaowa.fragment.FileManageFragment;
import com.bmobxiaowa.util.DialogImage;
import com.bmobxiaowa.util.FileUtul;
import com.flyco.animation.SlideEnter.SlideBottomEnter;
import com.flyco.dialog.utils.CornerUtils;
import com.flyco.dialog.widget.base.BaseDialog;

/**
 * 
 * Created by hanWG on 2015-11-27 上午9:25:51 $
 * 
 * @Description 单一上传操作
 *              <p/>
 */
public class UpLoadDialog extends BaseDialog {

	private FileManageFragment fragment;
	private MainActivity aty;
	private ImageView iamge;
	private String ur;
	private DialogImage dialogi;
	private KpaiZ kpz;
	private TextView tv;
	private K_LoadingCircleView iamge_loading_f;
	private boolean flag=true;
	private DaoService daoservcie;
	public UpLoadDialog(Context context) {
		super(context);
	}

	public UpLoadDialog(Context context, View view, FileManageFragment fragment) {
		super(context);
		this.fragment = fragment;
		aty = (MainActivity) context;

	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView() {
		// 注册EventBus
		EventBus.getDefault().register(this);
		setCanceledOnTouchOutside(false);
		widthScale(0.95f);
		heightScale(0.95f);
		showAnim(new SlideBottomEnter());

		View inflate = View.inflate(context, R.layout.fragment_image, null);
		Button button = ViewFindUtils.find(inflate, R.id.button1);
		iamge_loading_f = ViewFindUtils.find(inflate, R.id.iamge_loading_f);
		iamge_loading_f.setProgressColor(Color.MAGENTA);
		iamge_loading_f.setRingColor(Color.LTGRAY);
		iamge_loading_f.setRingWidthDip(5);

		tv = ViewFindUtils.find(inflate, R.id.tv);
		Button button2 = ViewFindUtils.find(inflate, R.id.button2);
		iamge = ViewFindUtils.find(inflate, R.id.iamge);
		inflate.setBackgroundDrawable(CornerUtils.cornerDrawable(
				Color.parseColor("#ffffff"), dp2px(5)));
		button.setOnClickListener(new KNoDoubleClickListener() {
			@Override
			public void onNoDoubleClick(View v) {
				dialogi = new DialogImage();
				kpz = new KpaiZ();
				Dialog dialog = dialogi.showDialog(fragment, aty, kpz);
				dialog.show();
			}
		});
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (flag) {
					aty.dialog.show();
				}
				KLoger.d("==上传==", "上传uri--"+ur);
				aty.dialog_tv.setText("正在上传");
				BTPFileResponse response = BmobProFile
						.getInstance(getContext()).upload(ur,
								new UploadListener() {
							

									@Override
									public void onSuccess(String fileName,
											String url, BmobFile file) {
										KToast.showToast(aty, "上传成功！",
												Toast.LENGTH_SHORT);
										aty.dialog.dismiss();
										//上传成功后把文件地址存放到数据库等待上传
										daoservcie = new DaoService(aty);
										// 把上传的文件名存入到文件
										Photo photo=new Photo();
										photo.setPhotoUrl(file.getFileUrl(aty));
										photo.setPhotoName(fileName);
										photo.setPhotoSort(FileUtul.getFileSuff(file.getFileUrl(aty)));
										if (photo!=null) {
											daoservcie.savePhoto(photo);
										}
										
									}

									@Override
									public void onProgress(int progress) {
										KLoger.i("==bmob图片访问==", "onProgress :"
												+ progress);
										if (progress==100) {
											iamge_loading_f.setVisibility(View.GONE);
										}else{
											iamge_loading_f.setVisibility(View.VISIBLE);
										}
										iamge_loading_f.setProgress(progress);
										
									}

									@Override
									public void onError(int statuscode,
											String errormsg) {
										KToast.showToast(aty, "上传成功！",
												Toast.LENGTH_SHORT);
										aty.dialog.dismiss();
									}
								});

			}
		});
		return inflate;
	}

	@Override
	public boolean setUiBeforShow() {
		return false;
	}

	@Override
	protected void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
		if (daoservcie!=null) {
			daoservcie.close();
		}
	}
	@Subscriber(tag = "xuanze")
	public void xuanze(String xuanze) {
		ur = xuanze;
		KGlide.Load(aty, iamge, xuanze);
	}

	@Subscriber(tag = "paizhao")
	public void paizhao(String paizhao) {
		ur = kpz.theLarge;
		KGlide.Load(aty, iamge, ur);
	}

	@Subscriber(tag = "qita")
	public void qita(String qita) {
		flag=false;
		ur = qita;
		tv.setText("您选择的文件地址是:+" + ur);
	}

}
