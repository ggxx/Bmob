package com.bmobxiaowa.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.simple.eventbus.EventBus;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bmobxiaowa.R;
import com.bmobxiaowa.adapter.SearchExpandableAdapter;
import com.bmobxiaowa.view.dialogview.UpLoadDialog;
import com.bmobxiaowa.view.dialogview.ViewFindUtils;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.FadeExit.FadeExit;
import com.flyco.animation.FlipEnter.FlipVerticalSwingEnter;

/**
 * Created by hanWG on 2015-11-24 下午3:36:13 $
 * 
 * @Description ExpandableListView封装外部调用
 *              <p/>
 *              传入父级数据和子数据<b>回调事件</b>
 */
public class ExpLayout extends LinearLayout {
	private BaseAnimatorSet bas_in;// 进入动画
	private BaseAnimatorSet bas_out;// 退出动画

	private List<Map<String, Object>> list;
	private String[] text_array;// 一级数据
	private String[][] child_text_array;// 二级数据
	private ExpandableListView listView;
	private SearchExpandableAdapter adapter;
	Activity aty;
	private String flag;// fgagment标记

	/**
	 * @param context
	 * @param yiji
	 * @param erji
	 */
	public ExpLayout(Context context, String[][] erji, String[] yiji,
			String flag) {
		super(context);
		child_text_array = erji;
		text_array = yiji;
		initView(context);
		this.flag = flag;
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public ExpLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	/**
	 * @param context
	 */
	private void initView(Context context) {
		aty = (Activity) context;
		bas_in = new FlipVerticalSwingEnter();
		bas_out = new FadeExit();
		setBackgroundColor(Color.parseColor("#ffffff"));
		View view = LayoutInflater.from(context).inflate(R.layout.explayout,
				this, false);
		listView = (ExpandableListView) view.findViewById(R.id.explayout);
		initModle(context);
		setListener();
		addView(view);
	}

	/**
	 * 初始化数据
	 */
	private void initModle(Context context) {
		list = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < text_array.length; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("txt", text_array[i]);
			list.add(map);
		}
		adapter = new SearchExpandableAdapter(context, list, child_text_array);
		listView.setAdapter(adapter);
		listView.expandGroup(0);  
	}

	// 点击事件
	private void setListener() {
		listView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {

				return false;
			}
		});

		// item 点击事件
		listView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				HashMap<String, Integer> map = new HashMap<String, Integer>();
				map.put("group", groupPosition);
				map.put("child", childPosition);
				EventBus.getDefault().post(map, flag);

				return false;
			}
		});
	}

}
