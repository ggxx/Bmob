package com.bmobxiaowa.service;

import han.k.common.KLoger;

import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import cn.bmob.v3.BmobRealTimeData;
import cn.bmob.v3.listener.ValueEventListener;

/**
 * Created by hanWG on 2015-12-2 下午3:45:33 $
 * 
 * @Description <p/>
 */
public class MyService extends Service {
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	@Override
	public void onCreate() {
		super.onCreate();

		final BmobRealTimeData rtd = new BmobRealTimeData();
		rtd.start(this, new ValueEventListener() {
			@Override
			public void onDataChange(JSONObject data) {
				KLoger.d("==bmob==", "(" + data.optString("action") + ")"
						+ "数据：" + data);
			}

			@Override
			public void onConnectCompleted() {
				KLoger.d("==bmob==", "连接成功:" + rtd.isConnected());
				if(rtd.isConnected()){
				    // 监听表更新
				    rtd.subTableUpdate("Photo");
				}
			}
		});
		
	}

}
