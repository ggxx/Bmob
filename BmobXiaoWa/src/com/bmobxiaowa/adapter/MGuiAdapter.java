package com.bmobxiaowa.adapter;

import han.k.common.ViewHolder;

import java.util.ArrayList;

import android.content.Context;
import android.widget.TextView;

import com.bmobxiaowa.R;
import com.bmobxiaowa.entity.Gui;

/**
 * Created by hanWG on 2015-11-11 下午4:19:56 $
 * 
 * @Description <p/>
 */
public class MGuiAdapter extends ListBaseAdapter<Gui> {
	/**
	 * @param ctx
	 * @param base
	 * @param itemVIewId
	 */
	public MGuiAdapter(Context ctx, ArrayList<Gui> base, int itemVIewId) {
		super(ctx, base, itemVIewId);
	}
	@Override
	public void converrt(ViewHolder holder, Gui t) {
		((TextView) holder.getView(R.id.title)).setText(t.getGtitle());
		((TextView) holder.getView(R.id.context)).setText(t.getGcontext());
	}

}
