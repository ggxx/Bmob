package com.bmobxiaowa.adapter;

import han.k.common.KAdapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

/**
 * Created by hanWG on 2015-11-13 下午5:43:09 $
 * 
 * @Description <p/>
 */
public abstract class ListBaseAdapter<T> extends KAdapter<T> {

	public ListBaseAdapter(Context ctx, ArrayList<T> base, int itemVIewId) {
		super(ctx, base, itemVIewId);
	}

	// 数据刷新数据
	public void addData(List<T> data) {
		if (mBase != null && data != null && !data.isEmpty()) {
			mBase.addAll(data);
			notifyDataSetChanged();
		}
	}

}
