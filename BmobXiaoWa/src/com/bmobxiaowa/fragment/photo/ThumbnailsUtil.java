package com.bmobxiaowa.fragment.photo;

import java.util.HashMap;

/**
 * Created by hanWG on 2015-11-30 下午1:56:23 $
 * 
 * @Description 存放图片类
 *              <p/>
 *              以key value键值对方式存放
 */
public class ThumbnailsUtil {
	private static HashMap<Integer, String> hash = new HashMap<Integer, String>();

	/**
	 * 返回value
	 * 
	 * @param key
	 * @return
	 */
	public static String MapgetHashValue(int key, String defalt) {
		if (hash == null || !hash.containsKey(key))
			return defalt;
		return hash.get(key);
	}

	/**
	 */
	public static void put(Integer key, String value) {
		hash.put(key, value);
	}

	public static void clear() {
		hash.clear();
	}
}
