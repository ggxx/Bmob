package com.bmobxiaowa.fragment.photo;

import han.k.base.BaseFragment;
import han.k.common.KLoger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.simple.eventbus.EventBus;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Thumbnails;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.bmobxiaowa.R;

/**
 * Created by hanWG on 2015-11-30 下午1:39:51 $
 * 
 * @Description 获取手机所有图片以列表方式展示
 *              <p/>
 */
public class PhotoFolderFragment extends BaseFragment {
	private ListView listView;
	private LinearLayout loadingLay;
	private ContentResolver cr;
	// 获取到本地所有图片的list
	private List<AlbumInfo> listImageInfo = new ArrayList<AlbumInfo>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			PhotoFolderAdapter listAdapter = new PhotoFolderAdapter(
					getActivity(), listImageInfo);
			listView.setAdapter(listAdapter);

		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater
				.inflate(R.layout.fragment_photofolder, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listView = (ListView) getView().findViewById(R.id.listView);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				List<PhotoInfo> data = listImageInfo.get(position).getList();
				EventBus.getDefault().post(data);
			}
		});
		// 获取内容提供者
		cr = getActivity().getContentResolver();
		ThumbnailsUtil.clear();
		listImageInfo.clear();

		// -----------------------------------线程获取本地图片
		new Thread() {
			public void run() {
				ThumbnailsUtil.clear();
				/************************ 获取缩略图 *****************************/
				String[] projection = { Thumbnails._ID, Thumbnails.IMAGE_ID,
						Thumbnails.DATA };
				Cursor cur = cr.query(Thumbnails.EXTERNAL_CONTENT_URI,
						projection, null, null, null);
				if (cur != null && cur.moveToFirst()) {
					int image_id;
					String image_path;
					int image_idColumn = cur
							.getColumnIndex(Thumbnails.IMAGE_ID);
					int dataColumn = cur.getColumnIndex(Thumbnails.DATA);
					do {
						image_id = cur.getInt(image_idColumn);
						image_path = cur.getString(dataColumn);
						KLoger.d("--图片地址--", image_path);
						ThumbnailsUtil.put(image_id, image_path);
					} while (cur.moveToNext());
				}

				/************************ 获取原图 *****************************/

				// 获取原图
				Cursor cursor = cr.query(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null,
						null, null, "date_modified DESC");
				String _path = "_data";
				String _album = "bucket_display_name";
				HashMap<String, AlbumInfo> myhash = new HashMap<String, AlbumInfo>();
				AlbumInfo albumInfo = null;
				PhotoInfo photoInfo = null;
				if (cursor != null && cursor.moveToFirst()) {
					do {
						int index = 0;
						int _id = cursor.getInt(cursor.getColumnIndex("_id"));
						String path = cursor.getString(cursor
								.getColumnIndex(_path));
						KLoger.d("--原来的图片地址--", path);
						String album = cursor.getString(cursor
								.getColumnIndex(_album));
						List<PhotoInfo> stringList = new ArrayList<PhotoInfo>();
						photoInfo = new PhotoInfo();
						if (myhash.containsKey(album)) {
							albumInfo = myhash.remove(album);
							if (listImageInfo.contains(albumInfo))
								index = listImageInfo.indexOf(albumInfo);
							photoInfo.setImage_id(_id);
							photoInfo.setPath_file( path);
							photoInfo.setPath_absolute(path);
							albumInfo.getList().add(photoInfo);
							listImageInfo.set(index, albumInfo);
							myhash.put(album, albumInfo);
						} else {
							albumInfo = new AlbumInfo();
							stringList.clear();
							photoInfo.setImage_id(_id);
							photoInfo.setPath_file(path);
							photoInfo.setPath_absolute(path);
							stringList.add(photoInfo);
							albumInfo.setImage_id(_id);
							albumInfo.setPath_file(path);
							albumInfo.setPath_absolute(path);
							albumInfo.setName_album(album);
							albumInfo.setList(stringList);
							listImageInfo.add(albumInfo);
							myhash.put(album, albumInfo);
						}
					} while (cursor.moveToNext());
				}
				Message msg = handler.obtainMessage();
				handler.sendMessage(msg);

			};
		}.start();

	}

}
