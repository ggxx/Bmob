package com.bmobxiaowa.fragment.photo;

import han.k.common.KGlide;
import han.k.common.KLoger;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bmobxiaowa.BmoboApplication;
import com.bmobxiaowa.R;

/**
 * Created by hanWG on 2015-11-30 下午3:51:12 $
 * 
 * @Description 相册适配器
 *              <p/>
 */
public class PhotoAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<PhotoInfo> list;
	private ViewHolder viewHolder;
	private GridView gridView;
	private Context context;

	public PhotoAdapter(Context context, List<PhotoInfo> list, GridView gridView) {
		mInflater = LayoutInflater.from(context);
		this.list = list;
		this.gridView = gridView;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * 刷新view
	 * 
	 * @param index
	 */
	public void refreshView(int index) {
		int visiblePos = gridView.getFirstVisiblePosition();
		View view = gridView.getChildAt(index - visiblePos);
		ViewHolder holder = (ViewHolder) view.getTag();

		if (list.get(index).isChoose()) {
			holder.selectImage.setImageResource(R.drawable.gou_selected);
		} else {
			holder.selectImage.setImageResource(R.drawable.gou_normal);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_selectphoto, null);
			viewHolder.image = (ImageView) convertView
					.findViewById(R.id.imageView);
			viewHolder.selectImage = (ImageView) convertView
					.findViewById(R.id.selectImage);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (list.get(position).isChoose()) {
			viewHolder.selectImage.setImageResource(R.drawable.gou_selected);
		} else {
			viewHolder.selectImage.setImageResource(R.drawable.gou_normal);
		}
		final PhotoInfo photoInfo = list.get(position);
		if (photoInfo != null) {
			KGlide.LoadList(context, viewHolder.image, photoInfo.getPath_file());
		}
		return convertView;
	}

	public class ViewHolder {
		public ImageView image;
		public ImageView selectImage;
	}
}
