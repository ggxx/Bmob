package com.bmobxiaowa.fragment;

import han.k.base.BaseFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bmobxiaowa.R;
import com.bmobxiaowa.api.EventBusTag;
import com.bmobxiaowa.base.Model;
import com.bmobxiaowa.view.ExpLayout;

/**
 * Created by hanWG on 2015-11-24 下午3:09:51 $
 * 
 * @Description 数据单个操作
 *              <p/>
 */
public class DataDanGeFragment extends BaseFragment {

	// 创建对象 由自己决定
	public static DataDanGeFragment newInstance() {
		DataDanGeFragment fragment = new DataDanGeFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	private ExpLayout expLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_datatb, container, false);
		initView(view);
		return view;
	}

	/**
	 * @param view
	 */
	private void initView(View view) {
		Model model = new Model();
		LinearLayout layouview = (LinearLayout) view
				.findViewById(R.id.layoutview);
		String[][] erji = model.EXPANDABLE_MORELIST_TXT;// 二级菜单
		String[] yiji = model.EXPANDABLE_LISTVIEW_TXT;// 一级菜单
		expLayout = new ExpLayout(getActivity(), erji, yiji,
				EventBusTag.DATATBFRAGMENT);
		layouview.addView(expLayout);
	}

}
