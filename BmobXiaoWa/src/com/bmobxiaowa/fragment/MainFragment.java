package com.bmobxiaowa.fragment;

import han.k.base.BaseFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bmobxiaowa.R;

/**
 * Created by hanWG on 2015-11-11 下午1:34:26 $
 * 
 * @Description 主UI
 * 
 *              <p/>
 */
public class MainFragment extends BaseFragment {

	// 创建对象 由自己决定
	public static MainFragment newInstance() {
		MainFragment fragment = new MainFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		return view;
	}

}
