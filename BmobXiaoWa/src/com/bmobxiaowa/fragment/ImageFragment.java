package com.bmobxiaowa.fragment;

import han.k.base.BaseFragment;
import han.k.common.KGlide;
import han.k.common.KLoger;
import han.k.util.KFileUitl;
import han.k.util.KpaiZ;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import cn.bmob.v3.datatype.BmobFile;

import com.bmob.BTPFileResponse;
import com.bmob.BmobProFile;
import com.bmob.btp.callback.ThumbnailListener;
import com.bmob.btp.callback.UploadListener;
import com.bmobxiaowa.R;
import com.bmobxiaowa.activity.MainActivity;
import com.bmobxiaowa.util.DialogImage;

/**
 * Created by hanWG on 2015-11-11 下午1:34:26 $
 * 
 * @Description 主UI
 *              <p/>
 */
public class ImageFragment extends BaseFragment {

	// 创建对象 由自己决定
	public static ImageFragment newInstance() {
		ImageFragment fragment = new ImageFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	private DialogImage dialogi;
	private Dialog dialog;
	private ImageView iamge;
	private Button button2;
	private String ur;
	private MainActivity aty;
	private KpaiZ kpz;
	private ExpandableListView expanlistview;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image, container, false);
		init(view);
		return view;
	}

	/**
	 * @param view
	 */
	private void init(View view) {
	/*	dialogi = new DialogImage();
		kpz = new KpaiZ();
		aty = (MainActivity) getActivity();
		iamge = (ImageView) view.findViewById(R.id.iamge);
		button2 = (Button) view.findViewById(R.id.button2);
		Button button = (Button) view.findViewById(R.id.button1);
		dialog = dialogi.showDialog(ImageFragment.this, getActivity(),kpz);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.show();
			}
		});
		button2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pushfile();
			}
		});*/
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		dialog.dismiss();
		// 拍照
		if (requestCode == DialogImage.REQUEST_CODE_PHOTOGRAPH) {
			if (data == null) {
				ur = kpz.theLarge;
			}
		}
		// 选择图片
		if (data != null && requestCode == DialogImage.REQUEST_CODE_LOCPIC) {
			if (resultCode == Activity.RESULT_OK) {
				Uri uri = data.getData();
				ur = KFileUitl.getRealFilePath(getActivity(), uri);
				KGlide.Load(getActivity(), iamge, ur);
				//Bitmap maps = KImageUtils.getScaledBitmap(ur, 500, 200);
				//iamge.setImageBitmap(maps);
			} else {
			}
		}

	}

	private void pushfile() {
		aty.dialog.show();
		aty.dialog_tv.setText("正在上传");
		
		
		BTPFileResponse response = BmobProFile.getInstance(getActivity())
				.upload(ur, new UploadListener() {
					@Override
					public void onSuccess(String fileName, String url,
							BmobFile file) {
						toast_("上传成功！");
						aty.dialog.dismiss();
						BmobProFile.getInstance(getActivity()).submitThumnailTask(fileName, 1, new ThumbnailListener() {

				            @Override
				            public void onSuccess(String thumbnailName,String thumbnailUrl) {
				            	toast_("缩略图成功");
				            	KLoger.i("==bmob图片访问==", "thumbnailName :" + thumbnailName+"---"+thumbnailUrl);
				            }

				            @Override
				            public void onError(int statuscode, String errormsg) {
				            	toast_("缩略图失败");
				            	
				            }
				        });
					}

					@Override
					public void onProgress(int progress) {
						KLoger.i("==bmob图片访问==", "onProgress :" + progress);
					}

					@Override
					public void onError(int statuscode, String errormsg) {
						toast_("上传失败");
						aty.dialog.dismiss();
					}
				});
		
		
		
		
		
		

	}

}
