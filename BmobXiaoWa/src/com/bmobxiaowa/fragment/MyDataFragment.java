package com.bmobxiaowa.fragment;

import han.k.common.KGlide;
import han.k.common.KLoger;
import han.k.common.ViewHolder;

import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.adapter.ListBaseAdapter;
import com.bmobxiaowa.base.BaseListFragment;
import com.bmobxiaowa.entity.Photo;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.NormalListDialog;

/**
 * Created by hanWG on 2015-12-2 下午1:57:45 $
 * 
 * @Description listView我的数据
 *              <p/>
 */
public class MyDataFragment extends BaseListFragment<Photo> {
	// 创建对象 由自己决定
	public static MyDataFragment newInstance() {
		MyDataFragment fragment = new MyDataFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	private String[] stringItems;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_list;
	}

	@Override
	protected ListBaseAdapter<Photo> getListAdapter() {
		return new ListBaseAdapter<Photo>(getActivity(), list,
				R.layout.item_photo) {

			@Override
			public void converrt(ViewHolder holder, Photo t) {
				ImageView imageview = holder.getView(R.id.imageview);
				KGlide.Load(getActivity(), imageview, t.getPhotoUrl());

			}
		};
	}
	@Override
	protected void initViews(View view) {
		error_layout.setVisibility(View.GONE);
		TextView tv=(TextView) view.findViewById(R.id.tv);
		//弹出筛选
		tv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectlist();
			}
		});
		

	}

	
	
	
	private void selectlist(){
		//只返回Person表的objectId这列的值
		BmobQuery<Photo> bmobQuery = new BmobQuery<Photo>();
		bmobQuery.addQueryKeys("photoSort");
		bmobQuery.findObjects(getActivity(), new FindListener<Photo>() {
		    @Override
		    public void onSuccess(List<Photo> object) {
		    	showDialog(object);
		    }
		    @Override
		    public void onError(int code, String msg) {
		    	toast_(msg);
		    }
		});
	}
	
	private void showDialog(List<Photo> object){
		stringItems =new String[object.size()];
		for (int i = 0; i < object.size(); i++) {
			KLoger.d("==chaxun===", object.get(i).getPhotoSort());
			stringItems[i]=object.get(i).getPhotoSort();
		}
		final NormalListDialog dialog = new NormalListDialog(getActivity(), stringItems);
		dialog.title("请选择")//
				.isTitleShow(false)//
				.itemPressColor(Color.parseColor("#85D3EF"))//
				.itemTextColor(Color.parseColor("#303030"))//
				.itemTextSize(15)//
				.cornerRadius(2)//
				.widthScale(0.75f)//
				.show();

		dialog.setOnOperItemClickL(new OnOperItemClickL() {
			@Override
			public void onOperItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String posit = stringItems[position];
				BmobQuery<Photo> query = new BmobQuery<Photo>();
				query.addWhereEqualTo("photoSort", posit);
				query.findObjects(getActivity(), new FindListener<Photo>() {
					@Override
					public void onSuccess(List<Photo> object) {
						error_layout.setVisibility(View.GONE);
						loaderMore.loaderMoreFinish();
						adapter.addData(object);
					}

					@Override
					public void onError(int code, String msg) {
					}
				});
				dialog.dismiss();
			}
		});
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
