package com.bmobxiaowa.fragment;

import han.k.base.BaseFragment;
import han.k.common.KLoger;
import han.k.util.KFileUitl;

import java.util.HashMap;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bmobxiaowa.R;
import com.bmobxiaowa.api.EventBusTag;
import com.bmobxiaowa.base.Model;
import com.bmobxiaowa.util.DialogImage;
import com.bmobxiaowa.util.FileUtul;
import com.bmobxiaowa.view.ExpLayout;
import com.bmobxiaowa.view.dialogview.BatchUpLoadDialog;
import com.bmobxiaowa.view.dialogview.UpLoadDialog;
import com.bmobxiaowa.view.dialogview.ViewFindUtils;

/**
 * Created by hanWG on 2015-11-24 下午3:09:51 $
 * 
 * @Description 文件管理
 *              <p/>
 */
public class FileManageFragment extends BaseFragment {

	// 创建对象 由自己决定
	public static FileManageFragment newInstance() {
		FileManageFragment fragment = new FileManageFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	private ExpLayout expLayout;
	private String[] groupdata;
	private String[][] childdata;
	private View elv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// 注册EventBus
		EventBus.getDefault().register(this);
		View view = inflater
				.inflate(R.layout.fragment_datatb, container, false);
		initView(view);
		return view;
	}

	/**
	 * @param view
	 */
	private void initView(View view) {
		Model model = new Model();
		LinearLayout layouview = (LinearLayout) view
				.findViewById(R.id.layoutview);
		View decorView = getActivity().getWindow().getDecorView();
		 elv = decorView.findViewById(R.id.mainviews);
		groupdata = model.FileBaseData;// 一级菜单
		childdata = model.FileChildData;// 二级菜单
		expLayout = new ExpLayout(getActivity(), childdata, groupdata,
				EventBusTag.FILEMANAGEFRAGMENT);
		layouview.addView(expLayout);
	}

	// 点击事件
	@Subscriber(tag = EventBusTag.FILEMANAGEFRAGMENT)
	public void onchildItemClickListener(HashMap<String, Integer> map) {
		int group = map.get("group");//
		int child = map.get("child");//
		String[] groups = childdata[group];
		String childs = groups[child];
		// 单一上传操作
		if ("单一上传".equals(childs)) {
			final UpLoadDialog dialog = new UpLoadDialog(getActivity(), elv,
					this);
			dialog.show();//
		}
		// 单一上传操作
		else if ("批量上传".equals(childs)) {
			final BatchUpLoadDialog dialog = new BatchUpLoadDialog(
					getActivity(),elv);
			dialog.show();//
		}

	}

	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 拍照
		if (requestCode == DialogImage.REQUEST_CODE_PHOTOGRAPH) {
			if (data == null) {
				EventBus.getDefault().post("paizhao", "paizhao");
			}
		}
		// 选择的其他文件
		if (requestCode == DialogImage.REQUEST_CODE_HISIPIN) {
			if (resultCode == Activity.RESULT_OK) {
				Uri uri = data.getData();
				String ur = KFileUitl.getRealFilePath(getActivity(), uri);
				if (!("".equals(ur))) {
					toast_("您选择的是" + FileUtul.getFileSuff(ur) + "格式文件!");
					EventBus.getDefault().post(ur, "qita");
				}
			}

		}
		// 选择图片
		if (data != null && requestCode == DialogImage.REQUEST_CODE_LOCPIC) {
			if (resultCode == Activity.RESULT_OK) {
				Uri uri = data.getData();
				String ur = KFileUitl.getRealFilePath(getActivity(), uri);
				EventBus.getDefault().post(ur, "xuanze");
			} else {
			}
		}

	}

}
