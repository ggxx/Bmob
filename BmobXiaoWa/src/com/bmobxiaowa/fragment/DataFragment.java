package com.bmobxiaowa.fragment;

import han.k.base.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.listener.SaveListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.api.EventBusTag;
import com.bmobxiaowa.base.Model;
import com.bmobxiaowa.db.DaoService;
import com.bmobxiaowa.entity.Photo;
import com.bmobxiaowa.view.ExpLayout;
import com.flyco.animation.FadeExit.FadeExit;
import com.flyco.animation.FlipEnter.FlipVerticalSwingEnter;
import com.flyco.dialog.listener.OnBtnLeftClickL;
import com.flyco.dialog.listener.OnBtnRightClickL;
import com.flyco.dialog.widget.MaterialDialog;

/**
 * Created by hanWG on 2015-11-24 下午3:09:51 $
 * 
 * @Description 数据操作
 *              <p/>
 *              在BmobObject对象中提供了三种用于批量操作的方法，分别是insertBatch、updateBatch、
 *              deleteBatch,批量添加、更新、删除。
 */
public class DataFragment extends BaseFragment {

	// 创建对象 由自己决定
	public static DataFragment newInstance() {
		DataFragment fragment = new DataFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	private ExpLayout expLayout;
	private String[][] erji;
	private FlipVerticalSwingEnter bas_in;
	private FadeExit bas_out;
	private ArrayList<BmobObject> photo;
	private DaoService dao;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// 注册EventBus
		EventBus.getDefault().register(this);
		View view = inflater
				.inflate(R.layout.fragment_datatb, container, false);
		initView(view);
		return view;
	}

	/**
	 * @param view
	 */
	private void initView(View view) {
		bas_in = new FlipVerticalSwingEnter();
		bas_out = new FadeExit();
		Model model = new Model();
		LinearLayout layouview = (LinearLayout) view
				.findViewById(R.id.layoutview);
		erji = model.ChildDataC;// 二级菜单
		String[] yiji = model.DataC;// 一级菜单
		expLayout = new ExpLayout(getActivity(), erji, yiji,
				EventBusTag.DATAFRAGMENT);
		layouview.addView(expLayout);
	}

	@Subscriber(tag = EventBusTag.DATAFRAGMENT)
	public void itemOnClickData(HashMap<String, Integer> map) {
		int group = map.get("group");//
		int child = map.get("child");//
		String[] groups = erji[group];
		String childs = groups[child];
		if ("批量添加".equals(childs)) {
			MaterialDialog();
		}

	}

	private void MaterialDialog() {
		final MaterialDialog dialog = new MaterialDialog(getActivity());
		dialog.title("批量添加");
		 dao = new DaoService(getActivity());
		photo = dao.queryData();
		dialog.content("你现在有:" + photo.size() + "条数据未上传!是否上传至bmob?")//
				.btnText("取消", "确定")//
				.showAnim(bas_in)//
				.dismissAnim(bas_out)//
				.show();
		dialog.setOnBtnLeftClickL(new OnBtnLeftClickL() {
			@Override
			public void onBtnLeftClick() {
				dialog.dismiss();
			}
		});

		dialog.setOnBtnRightClickL(new OnBtnRightClickL() {
			@Override
			public void onBtnRightClick() {
				
				new BmobObject().insertBatch(getActivity(), photo, new SaveListener() {
				    @Override
				    public void onSuccess() {
				    	dao.deletePhoto();
				    	toast_("上传成功!");
				    }
				    @Override
				    public void onFailure(int code, String msg) {
				    	toast_("上传失败!");
				    }
				});
				dialog.dismiss();
			}
		});
	}

	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

}
