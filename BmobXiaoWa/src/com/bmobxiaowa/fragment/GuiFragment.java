package com.bmobxiaowa.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.bmobxiaowa.R;
import com.bmobxiaowa.adapter.MGuiAdapter;
import com.bmobxiaowa.api.AppUrl;
import com.bmobxiaowa.api.HttpApi;
import com.bmobxiaowa.base.BaseListFragment;
import com.bmobxiaowa.entity.Gui;
import com.bmobxiaowa.util.UIHelpers;

/**
 * Created by hanWG on 2015-11-11 下午1:34:26 $
 * 
 * @Description 主UI
 *              <p/>
 */
public class GuiFragment extends BaseListFragment<Gui> {

	// 创建对象 由自己决定
	public static GuiFragment newInstance() {
		GuiFragment fragment = new GuiFragment();
		Bundle bundle = new Bundle();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	protected void initViews(View view) {
		// 请求数据
		getd.getHttpHtml(AppUrl.GuiMain);
		// 设置list点击事件
		loaderMore.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 查看详情页面
				UIHelpers.showContexActivity(getActivity(), list.get(position));
			}
		});
	}

	// 加载数据操作
	@Override
	protected void sendRequestData() {
		super.sendRequestData();
		// 加载下一页
		getd.getHttpHtml(AppUrl.DUAN + "index_" + mCurrentPage + ".html");
	}

	@Override
	protected MGuiAdapter getListAdapter() {
		return new MGuiAdapter(getActivity(), list, R.layout.item_gui);
	}

	/**
	 * 处理返回的数据
	 * 
	 * @return
	 */
	@Override
	protected ArrayList<Gui> parseList(String is) {
		return HttpApi.getGuiList(is);
	}
}
