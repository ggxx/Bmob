package com.bmobxiaowa.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hanWG on 2015-12-2 上午8:59:41 $
 * 
 * @Description SqliteOpenHelper
 *              <p/>
 *              用于创建，删除数据库操作，创建操作的数据库的对象
 * 
 * 
 */
public class DataHelper extends SQLiteOpenHelper {
	private static final String DATABASENAME = "bmob_url_data.db";// 数据库名称
	private static final int VERSION = 1;// 数据库名称

	public DataHelper(Context context) {
		super(context, DATABASENAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// 创建表名
		db.execSQL("create table if not exists imageurl (photoUrl varchar(60), photoSort varchar(60),photoName varchar(60))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
