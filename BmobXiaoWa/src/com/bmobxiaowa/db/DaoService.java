package com.bmobxiaowa.db;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.bmob.v3.BmobObject;

import com.bmobxiaowa.entity.Photo;

/**
 * Created by hanWG on 2015-12-2 上午10:01:49 $
 * 
 * @Description <p/>
 */
public class DaoService {
	private SQLiteOpenHelper datahelper;
	private SQLiteDatabase sqlManager;

	public DaoService(Context ctx) {
		// 创建MySQLiteOpenHelper辅助类对象
		datahelper = new DataHelper(ctx);
		sqlManager = datahelper.getWritableDatabase();
	}

	// 添加图片数据
	public void savePhoto(Photo photo) {
		sqlManager
				.execSQL(
						"insert into imageurl(photoUrl,photoSort,photoName) values(?,?,?)",
						new Object[] { photo.getPhotoUrl(),
								photo.getPhotoSort(), photo.getPhotoName() });

	}
	// 上传成功删除数据
	public void deletePhoto() {
		sqlManager.execSQL("delete from imageurl");
	}

	// 从数据库中查询数据
	public ArrayList<BmobObject> queryData() {
		Photo vo = null;
		String sql = "SELECT  * from  imageurl";
		Cursor cursor = sqlManager.rawQuery(sql, null);
		ArrayList<BmobObject> list = new ArrayList<BmobObject>();
		while (cursor.moveToNext()) {
			vo = new Photo();
			vo.setPhotoUrl(cursor.getString(cursor.getColumnIndex("photoUrl")));
			vo.setPhotoSort(cursor.getString(cursor.getColumnIndex("photoSort")));
			vo.setPhotoName(cursor.getString(cursor.getColumnIndex("photoName")));
			list.add(vo);
		}
		return list;
	}

	// 关闭使用
	public void close() {
		sqlManager.close();
	}

}
