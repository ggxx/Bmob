package com.bmobxiaowa.base;

import han.k.base.BaseFragment;
import han.k.common.KLoger;
import han.k.widget.K_EmptyLayout;
import han.k.widget.loader.GetMoreListView;
import han.k.widget.loader.GetMoreListView.OnListViewLoaderMoreListener;
import han.k.widget.refresh.PtrFrameLayout;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bmobxiaowa.adapter.ListBaseAdapter;
import com.bmobxiaowa.api.GetHttp;
import com.bmobxiaowa.api.GetHttp.OnHttpHtmlStringListener;
import com.library.R;

public abstract class BaseListFragment<T> extends BaseFragment implements
		OnHttpHtmlStringListener {
	
	/**
	 * 3 提示信息
	 */
	protected LinearLayout layout_v;
	/**
	 * 刷新
	 */
	protected PtrFrameLayout refresh;
	/**
	 * 加载
	 */
	protected GetMoreListView loaderMore;
	/**
	 * 无数据
	 */
	public K_EmptyLayout error_layout;
	public ArrayList<T> list;

	// 布局
	@Override
	protected int getLayoutId() {
		return R.layout.fragment_pull_refresh_listview;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		list=new ArrayList<T>();
		View view = inflater.inflate(getLayoutId(), container, false);
		initView(view);
		return view;
	}

	protected int mCurrentPage = 1;
	// 接口请求
	protected GetHttp getd;
	protected ListBaseAdapter<T> adapter;

	private void initView(View view) {
		getd = new GetHttp();
		// 监听数据回调接口
		getd.setOnHttpHtmlStringListener(this);
		layout_v = (LinearLayout) view.findViewById(R.id.layout_v);
		// 下拉刷新
		refresh = (PtrFrameLayout) view.findViewById(R.id.refresh);
		// 上拉加载更多
		loaderMore = (GetMoreListView) view.findViewById(R.id.loaderMore);
		loaderMore.setHasMore();
		// 加载更多
		loaderMore
				.setOnListViewLoaderMoreListener(new OnListViewLoaderMoreListener() {
					@Override
					public void onLoaderMore() {
						loaderMore.setHasMore();
						new Handler().postDelayed(new Thread() {
							@Override
							public void run() {
								mCurrentPage++;
								sendRequestData();
							}
						}, 500);
					}
				});
		// 上拉刷新
		adapter = getListAdapter();
		// 设置适配器
		loaderMore.setAdapter(adapter);
		error_layout = (K_EmptyLayout) view.findViewById(R.id.error_layout);
		initViews(view);
	}

	/**
	 * 获取适配器
	 * 
	 * @return
	 */

	protected abstract ListBaseAdapter<T> getListAdapter();

	protected void sendRequestData() {
	}

	// 获取到的列表数据
	@Override
	public void onHttpDate(String string) {
		error_layout.setVisibility(View.GONE);
		loaderMore.loaderMoreFinish();
		ArrayList<T> list = parseList(string);
		KLoger.d("==获取到数据==", "-->>"+list.size());
		adapter.addData(list);
	}

	protected ArrayList<T> parseList(String is) {
		return null;
	};
	protected abstract void initViews(View view);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
