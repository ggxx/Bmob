package com.bmobxiaowa.base;

import han.k.base.BaseActivity;
import han.k.util.KPopView;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import cn.bmob.v3.Bmob;

import com.bmobxiaowa.R;
import com.bmobxiaowa.api.BmobApi;

/**
 * Created by hanWG on 2015-11-10 下午4:51:25 $
 * 
 * @Description 需要的公共方法，属性，
 *              <p/>
 */
public class BomBaseActivity extends BaseActivity {
	public Dialog dialog;
	public TextView dialog_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View views = LayoutInflater.from(this).inflate(R.layout.view_dialog,null);
		dialog_tv=(TextView)views.findViewById(R.id.dialogtv);
		KPopView popView = new KPopView();
		dialog = popView.getDialogView(this,  views, 80,260);
	}

}
