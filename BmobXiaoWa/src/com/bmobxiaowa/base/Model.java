package com.bmobxiaowa.base;

public class Model {

	public String[] EXPANDABLE_LISTVIEW_TXT = new String[] { "数据介绍", "添加数据",
			"查询数据", "修改数据", "删除数据", };
	public String[][] EXPANDABLE_MORELIST_TXT = {
			{ "目前为止，Bmob支持的数据类型：String、Integer、Float、Short、Byte、Double、Character、Boolean、Object、Array。"
					+ "同时也支持BmobObject、BmobDate、BmobGeoPoint、BmobFile特有的数据类型。"
					+ "一个数据对象（APP中创建的BmobObject类和子类）对应于Bmob后台的一个数据表。" },
			{ "添加测试" }, { "查询所有", "查询单条数据", "复合查询", "查询指定列", "BQL指定列" },
			{ "修改测试" }, { "删除测试" } };

	/*----------------------文件管理------------------------------*/

	public String[] FileBaseData = new String[] { "文件操作介绍", "新版文件管理" };
	public String[][] FileChildData = {
			{ "Bmob的文件管理经过迭代后目前有新旧两个版本，其中新版本可支持断点续传，并且传输过程使用自定义协议，整个传输过程更加安全，因此，推荐使用新版文件管理。" },
			{ "单一上传", "URL签名", "批量上传", "文件下载", "获取文件地址", "文件删除", "缓存目录",
					"缩略图处理", } };
	/*---------------------- 数据操作------------------------------*/

	public String[] DataC = new String[] { "批量数据操作", "批量操作" };
	public String[][] ChildDataC = {
			{ "在BmobObject对象中提供了三种用于批量操作的方法，分别是insertBatch、updateBatch、deleteBatch,批量添加、更新、删除。" },
			{ "批量添加", "批量更新", "批量删除" } };

}
